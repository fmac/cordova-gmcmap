var path = require('path')

module.exports = {
    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].template = path.resolve(__dirname, 'public/index.html')
                return args
            })
    },
    publicPath: '',
    outputDir: 'www'
}
