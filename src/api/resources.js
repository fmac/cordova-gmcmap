import axios from "axios"

const regEx = {
    e: /[\s.]*\[(.*)\][\s.]*/,
    html: /'<.*>'/,
    data: /'<.*>',/g,
    box: /"topic-info-box".*?>(.*?)<\/.*?>/,
    tube: /<br>Tube:\s(.+?)<br>/,
    reading: /"info-box-reading".*?>(.*?)<\/.*?>/,
    historyParamID: /historyData\.asp\?Param_ID=(.*?)[^\d]/,
    contactParamID: /contactMember.asp\?param_ID=(.*?)[^\d]/,
    acpm: /ACPM:([\d.]*)/,
    eqDoseRate: new RegExp("ACPM:(?:[\\d.]*)(?:&nbsp;){3}([\\d.\\w/]*)"),
    by: /<br>By:\s(.+?)<br>/,
    on: /<br>\son:\s([\d\s\w-:]+)/
}

function parseResponse (res) {
    return res
        .split(",\r")
        .slice(0, -1)
        .map(el => {
            // Strip `[` and `],\s`
            const e = regEx.e.exec(el)[1]
            // Extract HTML
            const html = regEx.html.exec(e)[0].slice(1, -1)
            // Get rid of HTML
            const data = e.replace(regEx.data, "").split(",")
            // Extract nice details
            const box = regEx.box.exec(html),
                  reading = regEx.reading.exec(html)[1],
                  historyParamID = regEx.historyParamID.exec(html),
                  contactParamID = regEx.contactParamID.exec(html)
            // More datails from `info-box-reading`
            const acpm = regEx.acpm.exec(reading),
                  eqDoseRate = regEx.eqDoseRate.exec(reading),
                  by = regEx.by.exec(reading),
                  on = regEx.on.exec(reading),
                  tube = regEx.tube.exec(reading)

            return {
                acpm: acpm ? acpm[1] : null,
                box: box ? box[1] : null,
                by: by ? by[1] : null,
                color: data[3].slice(2, -1).split("|"),
                contactParamID: contactParamID ? contactParamID[1] : null,
                cpm: parseInt(data[2]),
                eqDoseRate: eqDoseRate ? eqDoseRate[1] : null,
                historyParamID: historyParamID ? historyParamID[1] : null,
                lat: parseFloat(data[0]),
                lng: parseFloat(data[1]),
                on: on ? on[1] : null,
                reading: reading ? reading[1] : null,
                tube: tube ? tube[1] : null
            }
        })
}

function fetchPoints(
    dataRange,
    chunkSize = 100,
    conc = 10,
    offset = 0,
    tot = []
) {
    const a = []
    for (let i = 0; i < conc; i += 1) {
        a.push(
            axios.get(
                "https://gmcmap.com/AJAX_load_time.asp?OffSet=" +
                    (offset + chunkSize * i) +
                    "&Limit=" +
                    chunkSize +
                    "&dataRange=" +
                    dataRange +
                    "&timeZone=0"
            )
        )
    }

    return axios.all(a).then(allRes => {
        let a = []
        for (const r of allRes) {
            if (r.status === 200) {
                if (r.data.length > 0) {
                    a = a.concat(parseResponse(r.data))
                }
            } else {
                throw Error("Error while fetching data: " + r.status)
            }
        }

        if (a.length % chunkSize === 0 && a.length > 0) {
            return fetchPoints(
                dataRange,
                chunkSize,
                conc,
                offset + chunkSize * conc,
                tot.concat(a)
            )
        } else {
            return tot.concat(a)
        }
    })
}

export {
    fetchPoints
}
