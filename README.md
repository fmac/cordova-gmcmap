Project moved
===

**go to: [https://sr.ht/~fmac/gmcmap-app](https://sr.ht/~fmac/gmcmap-app)**

GMC Map
=======

## Download

[<img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png' height="80" />](https://play.google.com/store/apps/details?id=xyz.fmac.gmcmap)

[![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/fmac/donate)

---

## What

This is a [Cordova](https://cordova.apache.org/)-based unofficial client for retrieving and viewing data from the distributed ambient radioactivity monitoring service hosted at https://gmcmap.com/ by [GQ Electronics LLC](https://www.gqelectronicsllc.com/).

## Why

I'm a happy GQ Electronics costumer who owns a couple of dosimeters and I really like looking at measurements people are sharing from around the world.\
What I hate about the official website is that:

- the UI is not so great
- it is not as quick as an app to check out
- it embeds Google Maps

So I decided to make my own client.

## How

This client uses [Leaflet](https://leafletjs.com/) and [OpenStreetMap](https://www.openstreetmap.org), which according to me are hands down better choices than Google Maps.

---

## Build the app yourself

1. `git clone https://codeberg.org/fmac/cordova-gmcmap.git`
2. `cd cordova-gmcmap`
3. `mkdir www`
4. `npm i`
5. `cordova platform add android`
6. `npm run build`

Then `npm run run` to deploy to default target
