import createPersistedState from "vuex-persistedstate";
import Vue from 'vue'
import Vuex from 'vuex'
import {
    DEFAULT_AUTO_RELOAD_SECONDS,
    DEFAULT_CENTER,
    DEFAULT_DATA_RANGE,
    DEFAULT_SETTINGS,
    DEFAULT_ZOOM,
    VUEX_PERSISTED_STATE_KEY
} from '../constants'

Vue.use(Vuex)

const state = {
    autoReloadSeconds: 0,
    dataRange: 0,
    savedCenter: {},
    savedZoom: 0,
    settings: {},
    points: []
}

state.autoReloadSeconds = DEFAULT_AUTO_RELOAD_SECONDS
state.dataRange = DEFAULT_DATA_RANGE
Object.assign(state.savedCenter, DEFAULT_CENTER)
state.savedZoom = DEFAULT_ZOOM
Object.assign(state.settings, DEFAULT_SETTINGS)

const mutations = {
    setAutoReloadSeconds (state, sec) {
        state.autoReloadSeconds = sec
    },
    setDataRange (state, range) {
        state.dataRange = range
    },
    setSavedCenter (state, pos) {
        state.savedCenter = pos
    },
    setSavedZoom (state, zoom) {
        state.savedZoom = zoom
    },
    setSetting (state, { name, value }) {
        Vue.set(state.settings, name, value)
    },
    setPoints (state, points) {
        state.points = points
    }
}

const actions = {}

const modules = {}

export default new Vuex.Store({
    plugins: [createPersistedState({
        key: VUEX_PERSISTED_STATE_KEY,
        paths: [
            'autoReloadSeconds',
            'dataRange',
            'savedCenter',
            'savedZoom',
            'settings'
        ]
    })],
    state,
    mutations,
    actions,
    modules
})
