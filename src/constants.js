export const DEFAULT_AUTO_RELOAD_SECONDS = 300
export const DEFAULT_CENTER = { "lat": 45.66585, "lng": 12.24554 }
export const DEFAULT_DATA_RANGE = 1
export const DEFAULT_SETTINGS = { show_cpm_values: true }
export const DEFAULT_ZOOM = 3.0

export const VUEX_PERSISTED_STATE_KEY = "gmcmap:vuex_persisted_state"
